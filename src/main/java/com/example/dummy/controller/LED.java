package com.example.dummy.controller;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

public class LED {

	public static final GpioController gpio = GpioFactory.getInstance();
	public static final GpioPinDigitalOutput pin1 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_01, "LED1", PinState.LOW);
	public static final GpioPinDigitalOutput pin2 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_02, "LED2", PinState.LOW);
	public static final GpioPinDigitalOutput pin3 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_03, "LED3", PinState.LOW);
	public static final GpioPinDigitalOutput pin4 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_04, "LED4", PinState.LOW);
	public static final GpioPinDigitalOutput pin5 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_05, "LED5", PinState.LOW);
	public static final GpioPinDigitalOutput pin6 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_06, "LED6", PinState.LOW);
	public static final GpioPinDigitalOutput pin7 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_07, "LED7", PinState.LOW);
	public static final GpioPinDigitalOutput pin8 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_08, "LED8", PinState.LOW);
	public static final GpioPinDigitalOutput pin9 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_09, "LED9", PinState.LOW);
	public static final GpioPinDigitalOutput pin10 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_10, "LED10", PinState.LOW);
	public static final GpioPinDigitalOutput pin11 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_11, "LED11", PinState.LOW);
	static {
		pin1.setShutdownOptions(true, PinState.LOW);
		pin2.setShutdownOptions(true, PinState.LOW);
		pin3.setShutdownOptions(true, PinState.LOW);
		pin4.setShutdownOptions(true, PinState.LOW);
		pin5.setShutdownOptions(true, PinState.LOW);
		pin6.setShutdownOptions(true, PinState.LOW);
		pin7.setShutdownOptions(true, PinState.LOW);
		pin8.setShutdownOptions(true, PinState.LOW);
		pin9.setShutdownOptions(true, PinState.LOW);
		pin10.setShutdownOptions(true, PinState.LOW);
		pin11.setShutdownOptions(true, PinState.LOW);
	}
}
