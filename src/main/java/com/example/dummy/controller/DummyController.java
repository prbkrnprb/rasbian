package com.example.dummy.controller;

import static com.example.dummy.controller.LED.pin1;
import static com.example.dummy.controller.LED.pin10;
import static com.example.dummy.controller.LED.pin11;
import static com.example.dummy.controller.LED.pin2;
import static com.example.dummy.controller.LED.pin3;
import static com.example.dummy.controller.LED.pin4;
import static com.example.dummy.controller.LED.pin5;
import static com.example.dummy.controller.LED.pin6;
import static com.example.dummy.controller.LED.pin7;
import static com.example.dummy.controller.LED.pin8;
import static com.example.dummy.controller.LED.pin9;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DummyController {

	@RequestMapping("/hello")
	public void hello(@RequestParam("list")String list) {
		System.out.println(list);
		if(list.equals("0")) {
			reset();
		}else {
			reset();
			String[] pins = list.split(",");
			for(String pin : pins) {
				int p = Integer.parseInt(pin);
				switch(p) {
				case 1:
					pin1.high();
					break;
				case 2:
					pin2.high();
					break;
				case 3:
					pin3.high();
					break;
				case 4:
					pin4.high();
					break;
				case 5:
					pin5.high();
					break;
				case 6:
					pin6.high();
					break;
				case 7:
					pin7.high();
					break;
				case 8:
					pin8.high();
					break;
				case 9:
					pin9.high();
					break;
				case 10:
					pin10.high();
					break;
				case 11:
					pin11.high();
					break;
				}
			}
		}
	}
	private void reset() {

		pin1.low();
		pin2.low();
		pin3.low();
		pin4.low();
		pin5.low();
		pin6.low();
		pin7.low();
		pin8.low();
		pin9.low();
		pin10.low();
		pin11.low();
	}
}
